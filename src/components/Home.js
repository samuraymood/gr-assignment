import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { SET_SHELTERID, SET_VALUE } from '../reducers/formSlice';
import { getShelters } from '../reducers/shelterSlice';
import { useTranslation } from "react-i18next";
import Donation from './addons/Donation';
import DonationValue from './addons/DonationValue';
import SEO from './addons/SEO';

import style from './Home.module.css';


const Home = ({setPageHandler}) =>{

    const dispatch = useDispatch();

    const state = useSelector(state=>state.form);
    const shelters = useSelector(state=>state.shelters.list.shelters);
    const shelterID = state.shelterID;
    const value = state.value;

    const [selectShelter,setSelectshelter] = useState(shelterID);
    const [donation,setDonation]= useState(value);
    const [customDonation,setCustomDonation]=useState(value);


    const {t} = useTranslation();
    const history = useHistory();

    const {register , handleSubmit } = useForm({
    });

    useEffect(() =>{
        dispatch(getShelters())
    },[dispatch]);

    const setDonationHandler = (e) => {
        setDonation(e.target.value);
    }
    const setCustomDonationHandler = (e) => {
        setCustomDonation(e.target.value);
        setDonation(e.target.value);
    }

    const setSelectshelterHandler = (e) =>{
        setSelectshelter(e.target.value);
    }

    const nextHandler = (data) =>{
        if(donation!==""){
            dispatch(SET_SHELTERID(data.shelterID));
            dispatch(SET_VALUE(donation));
            setPageHandler("informations");
            history.push('/informations');
        }
    }
    return(
        <div className={style.home}>
        <SEO 
            title="Good boy"
            description="Toto je úvodná stránka ,kde môžte prispieť pre útulky."
            pathSlug={"/home"}
            keywords={["utulky","peso","čokl"]}
            og_img ={"/img/some.jpg"}
            og_title={"Hi"}
            og_description={"hello world"}
        />
        <h1 className={style.title}>{t("title")}</h1>
        <Donation 
            selectShelter={selectShelter}
            t={t}    
        />
        <form onSubmit={handleSubmit(nextHandler)}>
            <div className={style.shelter_labels}>
                <label htmlFor="shelterID" className={style.home_label}>{t("about")}</label>
                <label htmlFor="shelterID" className={style.home_label_reccomendation}>{t("optional")}</label>
            </div>
            <label className={style.inside} htmlFor="shelterID">{t("shelter")}</label>
            <select defaultValue={shelterID} {...register("shelterID")} id="shelterID" onChange={setSelectshelterHandler} >
                <option className={style.blank} value="">{t("select")}</option>
                {shelters && shelters.map(shelter => {
                    return(
                        <option className={style.black} value={shelter.id}>{shelter.name}</option>
                    )
                })}
            </select>
            <DonationValue 
                t={t}
                customDonation={customDonation}
                setDonationHandler={setDonationHandler}
                setCustomDonationHandler={setCustomDonationHandler}
                donation={donation}
            />
            <div className={style.btn_container}>
                <button className={donation==="" ? style.next_btn : style.next_btn_active}>{t("continue")}</button>
            </div>
        </form>
        </div>
    );
}

export default Home;