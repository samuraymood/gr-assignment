import React, { useState } from 'react';
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { SET_NAME, SET_LASTNAME , SET_EMAIL ,SET_PHONE,SET_PHONE_BEFORE } from '../reducers/formSlice';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useTranslation } from "react-i18next";
import * as yup from 'yup';

import Rooting from './addons/Rooting';

import style from './Informations.module.css';

import SEO from './addons/SEO';

const Informations = ({setPageHandler}) =>{

    
    const state=useSelector(state=>state.form);
    
    const[phoneBefore,setPhoneBefore]=useState(state.phoneBefore==="" ? "+421" : state.phoneBefore);

    const name = state.name;
    const lastName = state.lastName;
    const email = state.email;
    const phone = state.phone;

    const schema = yup.object().shape({
        name : yup.string().matches(/^(|.{2,30})$/),
        lastName : yup.string().required().min(2).max(30),
        email : yup.string().required().email(),
        phone : yup.number().required()
    });

    const {register , handleSubmit , errors } = useForm({
        resolver : yupResolver(schema),
    });
    
    const {t} = useTranslation();

    const dispatch = useDispatch();

    let history = useHistory();

    //validations
    const backHandler = (e) =>{
        e.preventDefault();
        setPageHandler("home");
        history.push('/');
    }
    const nextHandler = (data) =>{
        dispatch(SET_NAME(data.name));
        dispatch(SET_LASTNAME(data.lastName));
        dispatch(SET_EMAIL(data.email));
        dispatch(SET_PHONE(data.phone));
        dispatch(SET_PHONE_BEFORE(phoneBefore));
        setPageHandler("confirm");
        history.push('/confirm');
    }

    const setPhoneBeforeHandler =()=>{
        if(phoneBefore==="+421"){
            setPhoneBefore("+420");
        }
        else{
            setPhoneBefore("+421");
        }
    }
    if(state.value){
    return(
        <>
        <SEO 
            title="Good boy - Vyplň informácie"
            description="Toto je úvodná stránka ,kde môžte prispieť pre útulky."
            pathSlug={"/informations"}
            keywords={["utulky","peso","čokl"]}
            og_img ={"/img/some.jpg"}
            og_title={"Hi"}
            og_description={"hello world"}
        />
        <h1 className={style.title}>{t("title2")}</h1>
        <form onSubmit={handleSubmit(nextHandler)}>
            <label className={style.inside} htmlFor="name">{t("name")}</label>
            <input type="text" defaultValue={name} placeholder="Zadajte Vaše meno" {...register("name")}/>
            <label className={style.inside} htmlFor="lastName">{t("lastname")}</label>
            <input type="text" defaultValue={lastName} {...register("lastName")} placeholder="Zadajte Vaše priezvisko"/>
            <label className={style.inside} htmlFor="email" >E-mailová adresa</label>
            <input type="email" defaultValue={email} {...register("email")} placeholder="Zadajte Váš e-mail"/>
            <div className={style.phone_container}>
                <div className={style.before} onClick={setPhoneBeforeHandler}>
                    <img src={phoneBefore==="+421" ? "img/flags/sk.png" : "img/flags/cz.png"} alt="Countries" /><span className={style.before_text}>{phoneBefore}</span>
                </div>
                <label className={style.inside} htmlFor="phone">{t("number")}</label>
                <input className={style.phone} type="text" defaultValue={phone} {...register("phone")}/>
            </div>
            <div className={style.btn_container}>
                <button className={errors ? style.next_btn : style.next_btn_active} >{t("continue")}</button>
                <button className={style.back_btn} onClick={backHandler}>{t("back")}</button>
            </div>
        </form>
        </>
    );
    }
    else{
        return(
          <Rooting style={style}/>
        )
      }
}

export default Informations;