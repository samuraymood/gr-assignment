import React, { useState } from 'react';
import { useHistory } from 'react-router';
import { useSelector } from 'react-redux';
import { useTranslation } from "react-i18next";
import axios from 'axios';
import swal from 'sweetalert';

import style from './Confirm.module.css';

import Info from './addons/Info';
import Rooting from './addons/Rooting';

import SEO from './addons/SEO';

const Confirm = ({setPageHandler}) =>{
    let history = useHistory();
    const state = useSelector(state=>state.form);
    const shelters = useSelector(state=>state.shelters.list.shelters);

    const [checked,setChecked]= useState(false);
    const {t} = useTranslation();

    const backHandler = (e) =>{
        history.push('/informations');
        e.preventDefault();
        setPageHandler("informations");
    }

    const goHome= () =>{
      history.push('/');
    }

    const submitHandler = () => {
      if(checked===true){
        axios.post('https://frontend-assignment-api.goodrequest.com/api/v1/shelters/contribute', {
            firstName: state.firstName,
            lastName: state.lastName,
            email : state.email,
            value : parseInt(state.value),
            phone : state.phoneBefore+state.phone,
            shelterID : state.shelterID ? parseInt(state.shelterID) : 0
          })
          .then(function (response) {
            swal({
              title: t("thanks"),
              text:  t("sent"),
              icon: "success",
              button: t("homepage"),
            }).then(()=>{
              goHome();
            }
            );
          })
          .catch(function (error) {
            swal({
              title: "Chyba",
              text: t("error") + " (" + error.message + " )",
              icon: "error",
              button: "Ok",
            })
          });
      }
      else{
        swal({
          text: t("acceptwarning"),
          button: t("acceptance"),
        });
      }
    }

    const setCheckedHandler = () =>{
      setChecked(!checked);
    }

    if(state.lastName){

    return(
        <div className={style.confirm_container}>
          <SEO 
            title="Good boy - Odošli formulár"
            description="Toto je úvodná stránka ,kde môžte prispieť pre útulky."
            pathSlug={"/home"}
            keywords={["utulky","peso","čokl"]}
            og_img ={"/img/some.jpg"}
            og_title={"Hi"}
            og_description={"hello world"}
        />
          <Info state={state} shelters={shelters} t={t}/>
          <div className={style.accept_container}>
            <input type="checkbox" name="spracovanie_udajov" id="spracovanie_udajov" onClick={setCheckedHandler}  />
            <span className={style.accept}>{t("accept")}</span>
          </div>
          <div className={style.btn_container}>        
            <button className={style.back_btn} onClick={backHandler}>{t("back")}</button>
            <button className={checked ? style.next_btn_active : style.next_btn} onClick={submitHandler}>{t("send")}</button>
          </div>
        </div>
    );
    }
    else{
      return(
        <Rooting style={style}/>
      )
    }
}

export default Confirm;