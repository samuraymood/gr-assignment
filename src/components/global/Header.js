import React from 'react';

import style from './Header.module.css';

const Header = () =>{
    return(
        <div className={style.header}>
            <div className={style.header_container}>
                <div className={style.title}>Nadácia GoodBoy</div>
                <div className={style.socials}>
                    <a href="www.google.sk"><img src="img/socials/fb.svg" alt="Facebook" /></a>
                    <a href="www.google.sk"><img src="img/socials/ig.svg" alt="Instagram" /></a>
                </div>
            </div>
        </div>
    );
}

export default Header;