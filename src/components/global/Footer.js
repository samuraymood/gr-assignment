import React from 'react';

import style from './Footer.module.css';

const Footer = () =>{
    return(
        <div className={style.footer}>
            <div className={style.footer_container}>
                <div className={style.logo}>
                    <img src="img/logo.svg" alt="Good Boy" />
                    <h1>Good Boy</h1>
                </div>
                <div className={style.menu}>
                    <div className={style.navigation}>
                        <div className={style.navigation_header}>
                            <b>Nadácia Good boy</b>
                        </div>
                        <div className={style.navigation_content}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus in interdum ipsum, sit amet.
                        </div>
                    </div>
                    <div className={style.navigation}>
                        <div className={style.navigation_header}>
                            <b>Nadácia Good boy</b>
                        </div>
                        <div className={style.navigation_content}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus in interdum ipsum, sit amet.
                        </div>
                    </div>
                    <div className={style.navigation}>
                        <div className={style.navigation_header}>
                            <b>Nadácia Good boy</b>
                        </div>
                        <div className={style.navigation_content}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus in interdum ipsum, sit amet.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Footer;