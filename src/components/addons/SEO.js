import { Helmet } from 'react-helmet';
import React from 'react';


const SEO = ({ title, description, pathSlug, keywords , og_img ,og_description ,og_title }) => {
    
    const base_url = window.location.origin;
    const url = base_url+pathSlug;
	
    return (
        <Helmet 
            htmlAttributes={{ lang: 'sk' }} 
            title={title} 
            meta={[
                {
                    name: 'description',
                    content: description,
                },
                {
                    name: 'keywords',
                    content: keywords.join(),
                },
                {
                    name:'og:title',
                    content : og_title,
                },
                {
                    name:'og:image',
                    content : og_description,
                },
                {
                    name:'og:image',
                    content : og_img,
                }
                ]}
                links={[
                    {
                        rel: 'canonical',
                        href: url,
                    },
                ]}
        />
    );
}

export default SEO;