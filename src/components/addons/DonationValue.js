import React from 'react';
import style from './DonationValue.module.css';

const DonationValue = ({customDonation,donation,setDonationHandler,setCustomDonationHandler,t}) =>{
    const isUsed=(value)=>{
        switch (value){
            case "":
                return false;
            case "5":
                return false;
            case "10" :
                return false;
            case "20" :
                return false;
            case "30" :
                return false;
            case "50" :
                return false;
            case "100" :
                return false;
            default :
                return true;
        }
    }
    return(
        <>
        <label style={{marginTop : 40 +"px"}} htmlFor="donation" className={style.home_label}>{t("donation")}</label>
            <div className={style.donation_container}>
                <div className={style.donation_wrap}>
                    <input type="text" style={{width:58+'px'}} readOnly value="5" className={donation==="5" ? style.my_checkbox_active : style.my_checkbox} onClick={setDonationHandler}/>
                    <span className={donation==="5" ? style.active : ""}>€</span>
                </div>
                <div className={style.donation_wrap}>
                    <input type="text" style={{width:64+'px'}} readOnly value="10" className={donation==="10" ? style.my_checkbox_active : style.my_checkbox} onClick={setDonationHandler}/>
                    <span className={donation==="10" ? style.active : ""}>€</span>
                </div>
                <div className={style.donation_wrap}>
                    <input type="text" style={{width:64+'px'}} readOnly value="20" className={donation==="20" ? style.my_checkbox_active : style.my_checkbox} onClick={setDonationHandler}/>
                    <span className={donation==="20" ? style.active : ""}>€</span>
                </div>
                <div className={style.donation_wrap}>
                    <input type="text" style={{width:64+'px'}} readOnly value="30" className={donation==="30" ? style.my_checkbox_active : style.my_checkbox} onClick={setDonationHandler}/>
                    <span className={donation==="30" ? style.active : ""}>€</span>
                </div>
                <div className={style.donation_wrap}>
                    <input type="text" style={{width:64+'px'}} readOnly value="50" className={donation==="50" ? style.my_checkbox_active : style.my_checkbox} onClick={setDonationHandler}/>
                    <span className={donation==="50" ? style.active : ""}>€</span>
                </div>
                <div className={style.donation_wrap}>
                    <input type="text" style={{width:75+'px'}} readOnly value="100" className={donation==="100" ? style.my_checkbox_active : style.my_checkbox} onClick={setDonationHandler}/>
                    <span className={donation==="100" ? style.active : ""}>€</span>
                </div>
                <div className={style.donation_wrap}>
                    <input type="text" style={{width:96+'px'}} value={customDonation} className={donation===customDonation && isUsed(donation) ? style.my_checkbox_active : style.my_checkbox} onChange={setCustomDonationHandler} onClick={setDonationHandler}/>
                    <span className={donation===customDonation && isUsed(donation) ? style.active : ""}>€</span>
                </div>
            </div>
        </>
    )
}

export default DonationValue;