import React from 'react';
import style from './Info.module.css';

const Info = ({shelters , state ,t}) =>{
    return(
        <>
        <h1>Skontrolujte si zadané údaje</h1>
          <div className={style.template}>
            <div className={style.template_content}>
              <h3>
                {t("type")}
              </h3>
              <p>
                {state.shelterID!=="" ? t("choosing.choose2") : t("choosing.choose1")}
              </p>
            </div>
            {state.shelterID!=="" &&
              <div className={style.template_content}>
                <h3>
                  {t("favourite")}
                </h3>
                <p>
                {shelters.filter(shelter => 
                    shelter.id===parseInt(state.shelterID)).map(filteredShelter =>
                      filteredShelter.name
                    )
                  }
                </p>
              </div>
            }
            <div className={style.template_content}>
              <h3>
                {t("value")}
              </h3>
              <p>
                {state.value + " €"}
              </p>
            </div>
            <div className={style.template_content}>
              <h3>
                {t("fullname")}
              </h3>
              <p>
                {state.name +" "+ state.lastName}
              </p>
            </div>
            <div className={style.template_content}>
              <h3>
                E-mailová adresa
              </h3>
              <p>
                {state.email}
              </p>
            </div>
            <div className={style.template_content}>
              <h3>
                {t("number")}
              </h3>
              <p>
                { state.phoneBefore+" "+state.phone}
              </p>
            </div>
          </div>
          </>
    )
}

export default Info;