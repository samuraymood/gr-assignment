import React from 'react';
import { useHistory } from 'react-router';
import { useTranslation } from "react-i18next";

const Rooting = ({style}) =>{

    let history = useHistory();
    const {t} = useTranslation();

    const goHome= () =>{
      history.push('/');
    }

    return(
        <div>
            <h1>{t("warning")}</h1>
            <button className={style.next_btn} onClick={goHome}>{t("homepage")}</button>
        </div>
    )
}

export default Rooting;