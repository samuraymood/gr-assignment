import React from 'react';
import style from './Donation.module.css';


const Donation = ({selectShelter,t}) =>{
    return(
        <div className={style.options}>
            <div className={selectShelter==="" ? style.first : style.first_option_active}>
                <div className={style.icon}>
                    <img src="img/icons/wallet.svg" alt="wallet" />
                </div>
                <p className={style.text}>
                {t("choosing.choose1")}
                </p>
            </div>
            <div className={selectShelter!=="" ? style.second : style.second_option_active}>
                <div className={style.icon}>
                    <img src="img/icons/paw.svg" alt="paw" />
                </div>
                <p className={style.text}>
                {t("choosing.choose2")}
                </p>
            </div>
        </div>
    )
}

export default Donation;