import { createSlice , createAsyncThunk } from '@reduxjs/toolkit';

export const getShelters = createAsyncThunk(
    'shelters/getShelters',
    async  () =>{
        return fetch('https://frontend-assignment-api.goodrequest.com/api/v1/shelters').then((result) => result.json());    
    }
);

const shelterSlice = createSlice({
    name : "shelters",
    initialState : {
        list: [],
        status:null,
    },
    extraReducers:{
        [getShelters.pending]: (state,action) => {
            state.status = 'loading'
        },
        [getShelters.fulfilled]: (state,{payload})=>{
            state.list = payload
            state.status = 'succcess'
        },
        [getShelters.rejected]: (state,action) => {
            state.status = 'failed'
        },
    }
    
});

export const shelterReducer = shelterSlice.reducer;