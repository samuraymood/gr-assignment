import { createSlice } from '@reduxjs/toolkit';

const formSlice = createSlice({
    name : "form",
    initialState : {
        name : "",
        lastName : "",
        shelterID: "",
        value : "",
        email: "",
        phoneBefore:"",
        phone : "",
    },
    reducers : {
      SET_NAME : (state, action) => {state.name = action.payload},  
      SET_LASTNAME : (state, action) => {state.lastName = action.payload}, 
      SET_SHELTERID : (state, action) => {state.shelterID = action.payload},
      SET_VALUE : (state, action) => {state.value = action.payload}, 
      SET_EMAIL : (state, action) => {state.email = action.payload},
      SET_PHONE_BEFORE : (state, action) => {state.phoneBefore = action.payload}, 
      SET_PHONE : (state, action) => {state.phone = action.payload}, 
    }  
});

export const formReducer = formSlice.reducer;

export const {
    SET_NAME,
    SET_LASTNAME,
    SET_SHELTERID,
    SET_VALUE,
    SET_EMAIL,
    SET_PHONE_BEFORE,
    SET_PHONE
} =formSlice.actions;