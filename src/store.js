import { configureStore } from '@reduxjs/toolkit';
import { shelterReducer } from './reducers/shelterSlice';
import { formReducer} from './reducers/formSlice';

export const store = configureStore({
    reducer : {
        shelters : shelterReducer,
        form : formReducer,
    }
});