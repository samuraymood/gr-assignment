import React, { useState , Suspense, useEffect } from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Home from './components/Home';
import Informations from './components/Informations';
import Confirm from './components/Confirm';
import swal from 'sweetalert';


import Header from './components/global/Header';
import Footer from './components/global/Footer';

import { useTranslation } from "react-i18next";

import style from './App.module.css';

function App() {
  const [page,setPage] = useState("home");
  const setPageHandler = (page) =>{
    setPage(page);
  }
  const { i18n } = useTranslation();

  const changeLanguage = (language) => {
    i18n.changeLanguage(language);
  };

  useEffect(()=>{
    swal("Vyber si jazyk", {
      buttons: {
        Slovensky: {
          value: "sk"
        },
        Česky: {
          value: "cz",
        },
      },
    }).then((value) => {
      switch (value) {
     
        case "cz":
          changeLanguage("cz")
          break;
     
        case "sk":
          changeLanguage("sk")
          break;
     
        default:
          changeLanguage("sk")
          break;
      }
    });
  },[]);

  return (
    <Suspense fallback={<div>Loading... </div>}>
    <div className={style.app}>
      <Header/>
      <div className={style.app_container}>
        <div className={style.layout}>
          <div className={style.layout_container}>
            <div className={style.page_container}>
              <span className={page==="home" ? style.page_active : style.page}>
              </span>
              <span className={page==="informations" ? style.page_active : style.page}>
              </span>
              <span className={page==="confirm" ? style.page_active : style.page}>
              </span>
            </div>
            <Router>
              <Route exact path="/">
                <Home setPageHandler={setPageHandler} />
              </Route>
              <Route path="/informations">
                <Informations setPageHandler={setPageHandler}/>
              </Route>
              <Route path="/confirm">
                <Confirm setPageHandler={setPageHandler}/>
              </Route>
            </Router>
          </div>
        </div>
        <div className={style.image}>
          <img src="img/dogo.png" alt="" />
        </div>
      </div>
      <Footer/>
    </div>
    </Suspense>
  );
}

export default App;
